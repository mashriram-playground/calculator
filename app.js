const operators = ["÷", "⨯", "+", "-"];
const addSubOperators = ["+", "-"];
const mulDivOperators = ["÷", "⨯"];
let inputArray = [];

const buttons = document.querySelectorAll(".button");
console.log(buttons);

/**
 * listening for button clicks
 */
addEventListenersToAllButtons(buttons);

/**THE MAIN FUNCTION (the handling of any button click)
 * @param {Event} e e:Event
 */
function onButtonClick(e) {
  let cleanedValue = e.target.innerHTML.replaceAll("&nbsp;", "");
  console.log("clicked: " + cleanedValue);

  let res = onInputClick(cleanedValue);
  console.log("res: ", res);

  const displayArea = document.getElementsByClassName("display-data")[0];
  displayArea.innerText = res.join("");
}
//
/**
 * END OF MAIN FUNCTION
 */

/**
 * #MAIN |
 * differentiating behaviours for different classes of buttons => called on each button click |
 * @param {string} inputValue inputValue:string
 * @returns inputArray:array
 */
function onInputClick(inputValue) {
  console.log("inside onInputClick: " + inputValue);

  if (inputValue === "Backspace") {
    inputArray.pop();
  } else if (inputValue === "C") {
    inputArray = [];
    console.log("cleared array " + JSON.stringify(inputArray));
  } else if (inputValue === "=") {
    console.log("got = as input.." + JSON.stringify(inputArray));
    // inputArray = calculateResult();
    resolvedArray = resolveInput(inputArray);
    operatorCleansedArray = removeTrailingOperatorsAndDots(resolvedArray);
    console.log("operatorCleansedArray: " + operatorCleansedArray);

    equalsArray = calculateResult(operatorCleansedArray);
    console.log("equalsarray", equalsArray);

    equalsArray = equalsArray.map((element) => element.toFixed(2));
    return equalsArray;
  } else {
    console.log("in else block");
    // console.log(isThereTwoDecimalsBetweenOperator(inputArray));
    if (isInvalidConsecutiveOperators(inputValue, inputArray)) {
      console.log("Invalid input , two operators given");
    } else if (
      inputValue === "." &&
      isThereTwoDecimalsBetweenOperator(inputArray)
    ) {
      console.log("two decimals given ignoring second");
    } else {
      console.log(inputValue);
      inputArray.push(inputValue);
    }
  }
  console.log("inputArray: ", inputArray);
  return inputArray;
}

/**
 * #PERIPHERAL |
 * used in onInputClick to check for invalid consecutive inputs |
 * @param {string} input input:string
 * @param {array} inputArray inputArray:array
 * @returns isInvalid:boolean
 */
function isInvalidConsecutiveOperators(input, inputArray) {
  let isInvalid = true;
  if (isCurrentInputAnOperator(input) && isLastInputAnOperator(inputArray)) {
    if (isCurrentAddSubOperator(input) && isLastMulDivOperator(inputArray)) {
      isInvalid = false;
    } else {
      isInvalid = true;
    }
  } else {
    isInvalid = false;
  }
  console.log("isInvalidConsecutiveOperators: ", isInvalid);
  return isInvalid;
}

/**
 * #PERIPHERAL |
 * @param {string} inpArray inpArray:string
 * @returns hasTwoDecimal:boolean
 */
function isThereTwoDecimalsBetweenOperator(inpArray) {
  let pointCount = 0;
  let hasTwodecimal = false;
  console.log(inpArray.length - 1);
  if (inpArray.length === 0) {
    return false;
  }
  for (
    let index = inpArray.length - 1;
    !operators.includes(inpArray[index]);
    index--
  ) {
    if (index >= 0) {
      console.log("pointcount" + pointCount);
      if (inpArray[index] === ".") {
        pointCount += 1;
      } else {
        break;
      }
    }
  }

  if (pointCount >= 1) {
    hasTwodecimal = true;
  } else {
    hasTwodecimal = false;
  }
  console.log(hasTwodecimal);
  return hasTwodecimal;
}

// end of receiving user input
/////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
//start of resolving and parsing user input

/**
 * #MAIN |
 * @param {array} inputArray inputArray:array
 * @returns resolvedInput:array
 */
function resolveInput(inputs) {
  let resolvedFloatString = "";
  let resolvedInput = [];
  let idx = 0;
  let operatorPosition = -1;
  for (const inp of inputs) {
    console.log("b4: ", inp);

    if (!operators.includes(inp)) {
      resolvedFloatString += inp;
      // } else if (idx === inputs.length - 1) {
      //   resolvedInput.push(parseFloat(resolvedFloatString));
      //   resolvedInput.push(inputs[inputs.length - 1]);
    } else {
      // idx = stringArray.indexOf(inp);
      operatorPosition = idx;
      console.log("resolvedFloatString: ", resolvedFloatString);
      if (resolvedFloatString.length > 0) {
        resolvedInput.push(parseFloat(resolvedFloatString));
      }
      console.log("currentElement: ", inp);
      resolvedInput.push(inp);
      console.log("resolvedInput: ", resolvedInput);
      resolvedFloatString = "";
    }
    idx += 1;
  }

  if (resolvedFloatString.length > 0) {
    resolvedInput.push(parseFloat(resolvedFloatString));
  }
  console.log("after push: ", resolvedInput);

  if (
    isLastResolvedInputAnOperator(resolvedInput) ||
    isLastResolvedInputADot(resolvedInput)
  ) {
    resolvedInput.pop();
  }
  return resolvedInput;
}

//#AUXILLARY Functions used in resolveInput()
//-----------------------------------------------------------------

function isLastResolvedInputAnOperator(resolvedInput) {
  return operators.includes(resolvedInput[resolvedInput.length - 1]);
}

function isLastResolvedInputADot(resolvedInput) {
  return resolvedInput[resolvedInput.length - 1] === ".";
}
//------------------------------------------------------------------
/**
 * Removes all trailing operators left after resolve input (it modifies the input array) |
 * @param {array} input input:array
 * @returns input:array
 */
function removeTrailingOperatorsAndDots(input) {
  let last = input[input.length - 1];
  if (operators.includes(last) || last === ".") {
    input.pop();
    return removeTrailingOperatorsAndDots(input);
  } else {
    return input;
  }
}

// end of resolving and parsing input
//////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
// start of calculation of result
/**
 * #MAIN
 * Calculation of division,multiplaication,addition,subtraction and returning expression |
 * @param {array} resolvedInp resolvedInp:array
 * @returns calculatedArray:array
 */
function calculateResult(resolvedInp) {
  console.log("inside calculateResult");
  resultArray = resolvedInp;
  console.log(resultArray);

  //===========================================================
  //start of multDiv evaluation
  //start of calculating division

  let result = [];
  while (resultArray.indexOf("÷") != -1) {
    console.log("inside division");
    idx = resultArray.indexOf("÷");
    console.log("idx", idx);
    if (
      resultArray[idx + 1] === 0 ||
      (operators.includes(resultArray[idx + 1]) && resultArray[idx + 2] === 0)
    ) {
      result = ["Division by Zero error"];
    } else if (isNextAddSubOperator(resultArray)) {
      if (resultArray[idx + 1] === "-") {
        calculation =
          parseFloat(resultArray[idx - 1]) / -parseFloat(resultArray[idx + 2]);
        resultArray = calculatedArray(
          calculation,
          resultArray,
          idx - 1,
          idx + 2
        );
      } else if (resultArray[idx + 1] === "+") {
        calculation =
          parseFloat(resultArray[idx - 1]) / parseFloat(resultArray[idx + 2]);
        resultArray = calculatedArray(
          calculation,
          resultArray,
          idx - 1,
          idx + 2
        );
      }
    } else {
      console.log("doing division");
      calculation =
        parseFloat(resultArray[idx - 1]) / parseFloat(resultArray[idx + 1]);
      resultArray = calculatedArray(calculation, resultArray, idx - 1, idx + 1);
    }
  }

  //end of calculating for division
  //-----------------------------------------------------------------------
  //start of calculating for multiplication

  while (resultArray.indexOf("⨯") != -1) {
    console.log("inside multiplication");
    idx = resultArray.indexOf("⨯");
    if (isNextAddSubOperator(resultArray)) {
      if (resultArray[idx + 1] === "-") {
        calculation =
          parseFloat(resultArray[idx - 1]) * -parseFloat(resultArray[idx + 2]);
        resultArray = calculatedArray(
          calculation,
          resultArray,
          idx - 1,
          idx + 2
        );
      } else if (resultArray[idx + 1] === "+") {
        calculation =
          parseFloat(resultArray[idx - 1]) * parseFloat(resultArray[idx + 2]);
        resultArray = calculatedArray(
          calculation,
          resultArray,
          idx - 1,
          idx + 2
        );
      }
    } else {
      calculation =
        parseFloat(resultArray[idx - 1]) * parseFloat(resultArray[idx + 1]);
      console.log("calculation: ", calculation);
      console.log("resultArray: ", resultArray);
      console.log("idx: ", idx);
      resultArray = calculatedArray(calculation, resultArray, idx - 1, idx + 1);
    }
  }

  //end of calculating for multiplication
  //end of mulDiv evaluation
  //==========================================================================
  //start of addSub evaluation
  //start of calculating for addition

  while (resultArray.indexOf("+") != -1) {
    console.log("inside addition");
    idx = resultArray.indexOf("+");
    calculation =
      parseFloat(resultArray[idx - 1]) + parseFloat(resultArray[idx + 1]);
    resultArray = calculatedArray(calculation, resultArray, idx - 1, idx + 1);
    // }
  }

  //end of calculating for addition
  //--------------------------------------------------------------------------
  //start of calculating for subtraction

  while (resultArray.indexOf("-") != -1) {
    console.log("inside subtraction");
    idx = resultArray.indexOf("-");
    calculation =
      parseFloat(resultArray[idx - 1]) - parseFloat(resultArray[idx + 1]);
    resultArray = calculatedArray(calculation, resultArray, idx - 1, idx + 1);
    // }
  }
  //end of calculating subtraction
  //end of addSub evaluation
  return resultArray;
  //==========================================================================
}

/**
 * #PERIPHERAL |
 * Used in calculateResult for simplifying the array elements after calculation |
 * calculateArrayt(calculation:int , resultArray:array , start_index:int , end_index:int ) => calcArray:array |
 * @param {int} calc calc:int
 * @param {array} array array:array
 * @param {int} start start:int
 * @param {int} end end:int
 * @returns calcArray:array
 */
function calculatedArray(calc, array, start, end) {
  console.log("start, end: ", start, end);
  let formerArray = array.slice(0, start);
  let latterArray = array.slice(end);
  latterArray.shift();
  let calcArray = [...formerArray, calc, ...latterArray];
  console.log("calcArray: ", calcArray);
  return calcArray;
}

//#AUXILLARY Functions used in onClick(),calculateResult()..etc
//--------------------------------------------------------------
function isLastInputAnOperator(inputArray) {
  const lastElement = inputArray[inputArray.length - 1];
  console.log("lastElement: ", lastElement);
  return operators.includes(lastElement);
}

function isNextAddSubOperator(inputArray) {
  const lastElement = inputArray[inputArray.length + 1];
  console.log("lastElement: ", lastElement);
  return addSubOperators.includes(lastElement);
}

function isCurrentInputAnOperator(input) {
  const isOperator = operators.includes(input);
  console.log("isOperator: ", isOperator);
  return isOperator;
}

function isCurrentAddSubOperator(input) {
  const isAddOrSub = addSubOperators.includes(input);
  console.log("isAddOrSub: ", isAddOrSub);
  return isAddOrSub;
}

function isLastMulDivOperator(inputArray) {
  return mulDivOperators.includes(inputArray[inputArray.length - 1]);
}

function addEventListenersToAllButtons(buttons) {
  for (const button of buttons) {
    button.addEventListener("click", onButtonClick);
  }
}
//----------------------------------------------------------------
